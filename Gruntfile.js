module.exports = function(grunt) {
  'use strict';

  const parentCWD  = grunt.option('parentCWD');
  const appName    = grunt.option('appName');
  const srcIndex   = `${parentCWD}/${grunt.option('srcIndex')}`;
  const buildDir   = `${parentCWD}/build/`;
  const tmpDir     = `${parentCWD}/.tmp/`;
  const buildIndex = `${buildDir}index.cvn`;
  const scripts    = require('./grunt/scriptGarner')(parentCWD, appName);
  const html       = require('./grunt/htmlGarner')(parentCWD);

  // Load the grunt tasks relative to this file.
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-filerev');
  grunt.loadNpmTasks('grunt-babel');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-filerev');
  grunt.loadNpmTasks('grunt-angular-templates');
  grunt.loadNpmTasks('grunt-usemin');

  // Run the tasks in the directory of the caller (where the build command was
  // executed).
  process.chdir(parentCWD);

  grunt.initConfig({
    clean:         require('./grunt/clean')(grunt, buildDir, tmpDir),
    jshint:        require('./grunt/jshint')(grunt, scripts),
    copy:          require('./grunt/copy')(grunt, srcIndex, buildIndex),
    useminPrepare: require('./grunt/useminPrepare')(grunt, srcIndex, buildDir),
    ngtemplates:   require('./grunt/ngtemplates')(grunt, appName, html, tmpDir),
    filerev:       require('./grunt/filerev')(grunt, buildDir),
    usemin:        require('./grunt/usemin')(grunt, buildIndex),
    babel:         require('./grunt/babel-ng')(grunt, tmpDir)
  });

  grunt.registerTask('default', [
    'clean',
    'jshint',
    'copy',
    'useminPrepare',
    'ngtemplates',
    'concat',
    'babel',
    'uglify',
    'cssmin',
    'filerev',
    'usemin',
    'clean:tmp'
  ]);
};

