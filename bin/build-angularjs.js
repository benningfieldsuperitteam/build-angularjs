#!/usr/bin/env node

'use strict';

const argv = require('yargs')
  .usage('Uage: $0 <options>')

  .option('angular-module-name', {
    describe: 'The name of your AngularJS module.'
  })

  .option('index-file', {
    default: 'index.html',
    describe: 'Base index file from which JavaScript files are included.'
  })

  .demandOption('angular-module-name')
  .argv;

const grunt = require('grunt');
const cwd   = process.cwd();

// Run the default task.
grunt.cli({
  gruntfile : `${__dirname}/../Gruntfile.js`,
  parentCWD : cwd,
  appName   : argv['angular-module-name'],
  srcIndex  : argv['index-file']
});

