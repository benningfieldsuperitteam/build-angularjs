module.exports = function(grunt, buildDir) {
  'use strict';

  const filerev = {
    js: {
      src: buildDir + 'js/*.js'
    },
    css: {
      src: buildDir + '**/*.css'
    }
  };

  return filerev;
};

