module.exports = function(grunt, srcIndex, buildIndex) {
  'use strict';

  const copy = {
    html: {
      src:  srcIndex,
      dest: buildIndex
    },
    fonts: {
      cwd:    './../../',
      src:    'fonts/*',
      dest:   'build/',
      expand: true
    }
  };

  return copy;
};

