module.exports = function(grunt, tmpDir) {
  'use strict';

  const presets2015 = `${__dirname}/../node_modules/babel-preset-es2015`;

  const babel = {
    options: {
      presets: [presets2015],
      ignore:  /lib.*/
    },
    dist: {
      files: [{
        expand: true,
        src: `${tmpDir}concat/js/*.js`
      }]
    }
  };

  return babel;
};

