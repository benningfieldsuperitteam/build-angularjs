module.exports = function(parentCWD, appName) {
  'use strict';

  const glob     = require('glob');
  const globOpts = {cwd: parentCWD};
  const entry    = `${appName}.js`;
  let   files    = [entry];

  files = files.concat(glob.sync('**/*.js', globOpts).filter(function(script) {
    return !script.match(/^node_modules/) &&
           !script.match(/^bower_components/) &&
           script !== entry &&
           !script.match(/^build/) &&
           !script.match(/^.tmp/) &&
           !script.match(/^grunt/i);
  }));

  console.log('Script garner gathered the following files.');
  console.log(files);
  return files;
};

