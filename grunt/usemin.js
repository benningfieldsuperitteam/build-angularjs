module.exports = function(grunt, buildIndex) {
  'use strict';

  const usemin = {
    html: [buildIndex]
  };

  return usemin;
};

