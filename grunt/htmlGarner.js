module.exports = function(parentCWD) {
  'use strict';

  const glob     = require('glob');
  const globOpts = {cwd: parentCWD};
  const files    = glob.sync('**/*.html', globOpts).filter(function(file) {
    return !file.match(/^node_modules/) &&
           !file.match(/^bower_components/) &&
           !file.match(/^build/) &&
           !file.match(/^index.html/) &&
           !file.match(/^.tmp/);
  });

  console.log('HTML garner gathered the following files.');
  console.log(files);
  return files;
};

