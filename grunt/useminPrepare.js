module.exports = function(grunt, srcIndex, buildDir) {
  'use strict';

  const useminPrepare = {
    html: srcIndex,
    options: {
      dest: buildDir
    }
  };

  return useminPrepare;
};

